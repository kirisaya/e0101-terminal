## 练习0101: 熟悉终端命令行基本的文件目录操作

### 视频讲解

从百度云盘或者阿里云盘可以下载本练习的视频讲解。

```
阿里云盘: https://www.aliyundrive.com/s/UsJcgScxtre
百度网盘: https://pan.baidu.com/s/12Fgas_9gZzJxh2n4UACibA?pwd=8q2r
```

### 任务1: 删除文件

请使用 `rm` 命令删除 `data/wrong.txt` 文件。

### 任务2: 删除文件夹

请使用 `rm` 命令删除 `folder_del` 文件夹及下的全部文件。

### 任务3: 复制文件

请使用 `cp` 命令将 `data` 文件夹下的 `plain.txt` 文件复制到 `script` 文件夹下。

### 任务4: 复制文件夹

请使用 `cp` 命令将 `data` 文件夹连带其下的全部文件复制为 `data_bak` 文件夹。

### 任务5: 创建文件夹

请使用命令行 (一行) 创建一个名为 `data_new` 的文件夹，其下包含 `tables` 文件夹。

### 任务6: 移动文件

请使用命令将 `data/table.csv` 文件移动至 `data_new/tables` 文件夹下。

### 任务7: 重命名文件

请使用命令将 `data/table.tsv` 文件重命名为 `data/table.tsv.bak`。

### 作业批改

- 运行 `pytest` 命令可以测试任务 1--7 是否正确完成
- 任务 1-7 全部完成之后，测试才会全部通过，因为任务 6、7 会影响任务 4 的结果

### 相关链接

- 上一课 - [准备开发环境](https://gqnotes.notion.site/01-7bb2f9403f0b451180f00a60822d17b0)
- 下一课 - [练习0201: 选择性地批量复制文件](https://gitee.com/cueb-fintech/e0201-find-xargs)

---

<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br />本作品由<a xmlns:cc="http://creativecommons.org/ns#" href="https://jrx.cueb.edu.cn/szll/jss/45281.htm" property="cc:attributionName" rel="cc:attributionURL">首都经济贸易大学-高强</a>采用<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">知识共享署名-禁止演绎 4.0 国际许可协议</a>进行许可。
